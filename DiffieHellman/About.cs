﻿using System.Windows.Forms;

namespace DiffieHellman
{
    public partial class About : Form
    {
        private MainForm _parent;
        public About(MainForm parent)
        {
            _parent = parent;
            InitializeComponent();
        }

        private void About_FormClosed(object sender, FormClosedEventArgs e)
        {
            _parent.SetAboutToolStripMenuEnabled();
        }
    }
}