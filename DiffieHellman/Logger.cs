﻿using System;
using System.IO;

namespace DiffieHellman
{
    public static class Logger
    {
        private static string _path;
        private static bool _active;
        public static void Log(string message)
        {
            if (!_active) return;
            
            using (var sw = GetStreamWriter())
            {
                sw.WriteLine(DateTime.Now.ToString("HH:mm:ss tt") + " " + message);
            }
        }

        private static StreamWriter GetStreamWriter()
        {
            return new StreamWriter(_path, true);
        }

        public static void Activate(string path)
        {
            _active = true;
            _path = path;
            using (File.Create(_path))
            {
            }
        }

        public static void Disactivate()
        {
            _active = false;
        }
    }
}