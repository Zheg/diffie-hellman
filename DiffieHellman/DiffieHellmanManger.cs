﻿namespace DiffieHellman
{
    public static class DiffieHellman
    {
        public const long Modulo = 700666007;
        public const long G = 5;

        public static long PowerModulo(long expBase, long exp) 
        {
            expBase %= Modulo;
            if (exp == 0) return 1;
            if (exp % 2 == 0) {
                return PowerModulo(expBase * expBase % Modulo, exp / 2);
            }

            return expBase * PowerModulo(expBase, exp - 1) % Modulo;
        }
    }
}