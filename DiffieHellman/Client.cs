﻿using System;
using System.Windows.Forms;

namespace DiffieHellman
{
    public partial class Client : Form
    {
        public readonly string UserName;
        private readonly long _clientSecretKey;
        private readonly long _clientOpenKey;
        private long _anotherClientOpenKey;
        private long _secretKey;

        public long AnotherClientOpenKey
        {
            private get { return _anotherClientOpenKey; }

            set
            {
                Logger.Log(UserName + " receive another client open key = " + _clientOpenKey);
                _anotherClientOpenKey = value;
                _secretKey = DiffieHellman.PowerModulo(_anotherClientOpenKey, _clientSecretKey);
                Logger.Log(UserName + " generate secret key = " + _secretKey);
            }
        }

        public Client(string name)
        {
            InitializeComponent();
            UserName = name;
            _clientSecretKey = new Random().Next();
            Logger.Log(UserName + " generate own secret key = " + _clientSecretKey);
            _clientOpenKey = DiffieHellman.PowerModulo(DiffieHellman.G, _clientSecretKey);
            Logger.Log(UserName + " generate own open key = " + _clientOpenKey);
            OwnOpenKey.Text = _clientOpenKey.ToString();
            Manager.Clients.Add(this);
        }

        public bool IsThisSecretKey(long key)
        {
            return key == _secretKey;
        }

        public void ReceiveMessage(string senderName, string message, DateTime time)
        {
            AddMessage("IN", senderName, message, time);
        }

        private void CreateConnectionButton_Click(object sender, EventArgs e)
        {
            AnotherClientOpenKey = (long) AnotherOpenKey.Value;
            MessengerPanel.Visible = true;
            this.Width = 600;
            this.Height = 480;
        }

        private void AnotherOpenKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateConnectionButton_Click(sender, e);
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DisconnectButton_Click(object sender, EventArgs e)
        {
            MessengerTextBox.Text = "";
            MessengerPanel.Visible = false;
            this.Width = 295;
            this.Height = 135;
        }

        private void SendMessageButton_Click(object sender, EventArgs e)
        {
            var message = InputTextBox.Text;
            AddMessage("OUT", UserName, message, DateTime.Now);
            Manager.SendMessage(_secretKey, this, message);
            InputTextBox.Text = "";
        }

        private void AddMessage(string type, string userName, string message, DateTime time)
        {
            MessengerTextBox.Text += userName + @": " + message + "    (" + time.ToString("HH:mm:ss tt") + ")\r\n";
        }

        private void InputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Alt)
            {
                SendMessageButton_Click(sender, e);
            }
        }

        private void InputTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            
        }
    }
}