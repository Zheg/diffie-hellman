﻿using System.ComponentModel;

namespace DiffieHellman
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.OwnOpenKey = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CreateConnectionButton = new System.Windows.Forms.Button();
            this.AnotherOpenKey = new System.Windows.Forms.NumericUpDown();
            this.MessengerPanel = new System.Windows.Forms.Panel();
            this.DisconnectButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.InputTextBox = new System.Windows.Forms.TextBox();
            this.SendMessageButton = new System.Windows.Forms.Button();
            this.MessengerTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize) (this.AnotherOpenKey)).BeginInit();
            this.MessengerPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Your open key:";
            // 
            // OwnOpenKey
            // 
            this.OwnOpenKey.Location = new System.Drawing.Point(167, 6);
            this.OwnOpenKey.Name = "OwnOpenKey";
            this.OwnOpenKey.ReadOnly = true;
            this.OwnOpenKey.Size = new System.Drawing.Size(100, 20);
            this.OwnOpenKey.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Input another client open key:";
            // 
            // CreateConnectionButton
            // 
            this.CreateConnectionButton.Location = new System.Drawing.Point(12, 61);
            this.CreateConnectionButton.Name = "CreateConnectionButton";
            this.CreateConnectionButton.Size = new System.Drawing.Size(255, 23);
            this.CreateConnectionButton.TabIndex = 4;
            this.CreateConnectionButton.Text = "Create connection";
            this.CreateConnectionButton.UseVisualStyleBackColor = true;
            this.CreateConnectionButton.Click += new System.EventHandler(this.CreateConnectionButton_Click);
            // 
            // AnotherOpenKey
            // 
            this.AnotherOpenKey.Location = new System.Drawing.Point(167, 33);
            this.AnotherOpenKey.Maximum = new decimal(new int[] {10000000, 0, 0, 0});
            this.AnotherOpenKey.Name = "AnotherOpenKey";
            this.AnotherOpenKey.Size = new System.Drawing.Size(100, 20);
            this.AnotherOpenKey.TabIndex = 5;
            this.AnotherOpenKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AnotherOpenKey_KeyDown);
            // 
            // MessengerPanel
            // 
            this.MessengerPanel.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.MessengerPanel.Controls.Add(this.DisconnectButton);
            this.MessengerPanel.Controls.Add(this.CloseButton);
            this.MessengerPanel.Controls.Add(this.InputTextBox);
            this.MessengerPanel.Controls.Add(this.SendMessageButton);
            this.MessengerPanel.Controls.Add(this.MessengerTextBox);
            this.MessengerPanel.Location = new System.Drawing.Point(12, 6);
            this.MessengerPanel.Name = "MessengerPanel";
            this.MessengerPanel.Size = new System.Drawing.Size(255, 78);
            this.MessengerPanel.TabIndex = 6;
            this.MessengerPanel.Visible = false;
            // 
            // DisconnectButton
            // 
            this.DisconnectButton.Location = new System.Drawing.Point(401, 397);
            this.DisconnectButton.Name = "DisconnectButton";
            this.DisconnectButton.Size = new System.Drawing.Size(75, 23);
            this.DisconnectButton.TabIndex = 4;
            this.DisconnectButton.Text = "Disconnect";
            this.DisconnectButton.UseVisualStyleBackColor = true;
            this.DisconnectButton.Click += new System.EventHandler(this.DisconnectButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Location = new System.Drawing.Point(177, 52);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // InputTextBox
            // 
            this.InputTextBox.Location = new System.Drawing.Point(3, 343);
            this.InputTextBox.Multiline = true;
            this.InputTextBox.Name = "InputTextBox";
            this.InputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.InputTextBox.Size = new System.Drawing.Size(554, 48);
            this.InputTextBox.TabIndex = 2;
            this.InputTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputTextBox_KeyDown);
            this.InputTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.InputTextBox_KeyUp);
            // 
            // SendMessageButton
            // 
            this.SendMessageButton.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SendMessageButton.Location = new System.Drawing.Point(3, 52);
            this.SendMessageButton.Name = "SendMessageButton";
            this.SendMessageButton.Size = new System.Drawing.Size(75, 23);
            this.SendMessageButton.TabIndex = 1;
            this.SendMessageButton.Text = "Send";
            this.SendMessageButton.UseVisualStyleBackColor = true;
            this.SendMessageButton.Click += new System.EventHandler(this.SendMessageButton_Click);
            // 
            // MessengerTextBox
            // 
            this.MessengerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.MessengerTextBox.Location = new System.Drawing.Point(3, 3);
            this.MessengerTextBox.Multiline = true;
            this.MessengerTextBox.Name = "MessengerTextBox";
            this.MessengerTextBox.ReadOnly = true;
            this.MessengerTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.MessengerTextBox.Size = new System.Drawing.Size(249, 0);
            this.MessengerTextBox.TabIndex = 0;
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(279, 96);
            this.Controls.Add(this.MessengerPanel);
            this.Controls.Add(this.AnotherOpenKey);
            this.Controls.Add(this.CreateConnectionButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OwnOpenKey);
            this.Controls.Add(this.label1);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Client";
            ((System.ComponentModel.ISupportInitialize) (this.AnotherOpenKey)).EndInit();
            this.MessengerPanel.ResumeLayout(false);
            this.MessengerPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button DisconnectButton;

        private System.Windows.Forms.Button button1;

        private System.Windows.Forms.Button SendMessageButton;
        private System.Windows.Forms.TextBox InputTextBox;

        private System.Windows.Forms.TextBox MessengerTextBox;

        private System.Windows.Forms.Panel MessengerPanel;

        private System.Windows.Forms.Panel panel1;

        private System.Windows.Forms.NumericUpDown AnotherOpenKey;

        private System.Windows.Forms.NumericUpDown numericUpDown1;

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button CreateConnectionButton;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OwnOpenKey;

        #endregion
    }
}