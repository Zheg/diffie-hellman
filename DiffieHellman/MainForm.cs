﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DiffieHellman
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void createNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var clientCreator = new ClientCreator();
            clientCreator.ShowDialog();
        }

        private void ActivateLogger_CheckedChanged(object sender, EventArgs e)
        {
            if (ActivateLogger.Checked)
            {
                var clearFile = MessageBox.Show("File will be cleared!", "Warning!", MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Warning);
                if (clearFile == DialogResult.Cancel)
                {
                    ActivateLogger.Checked = false;
                    return;
                }
                var openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = false;
                openFileDialog.CheckFileExists = true;
                openFileDialog.Filter = "Text file (*.txt)|*.txt";
                openFileDialog.FilterIndex = 0;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var path = openFileDialog.FileName;
                    Logger.Activate(path);
                }
            }
            else
            {
                Logger.Disactivate();   
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            aboutToolStripMenuItem.Enabled = false;
            var about = new About(this);
            about.Show();
        }

        public void SetAboutToolStripMenuEnabled()
        {
            aboutToolStripMenuItem.Enabled = true;
        }
    }
}