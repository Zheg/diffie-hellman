﻿using System;
using System.Windows.Forms;

namespace DiffieHellman
{
    public partial class ClientCreator : Form
    {
        public ClientCreator()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            var client = new Client(NameInput.Text);
            client.Show();
            Close();
        }

        private void LoggerActivate_CheckedChanged(object sender, EventArgs e)
        {
            
        }
    }
}