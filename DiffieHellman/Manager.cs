﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DiffieHellman
{
    public static class Manager
    {
        public static readonly List<Client> Clients = new List<Client>();

        public static void SendMessage(long key, Client sender, string message)
        {
            foreach (var client in Clients.Where(client => client.IsThisSecretKey(key) && client != sender))
            {
                client.ReceiveMessage(client.UserName, message, DateTime.Now);
                
                // only one receiver, so return
                return;
            }
        }
    }
}